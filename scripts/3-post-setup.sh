#!/usr/bin/env bash
# Foreground Colors
black='\e[30m'
green='\e[32m'
cyan='\e[36m'
lightred='\e[91m'
lightgreen='\e[92m'
lightyellow='\e[93m'
lightblue='\e[94m'
lightmagenta='\e[95m'
# Background Colors
RED='\e[41m'
LIGHTGREEN='\e[102m'
LIGHTYELLOW='\e[103m'
# Specials
BOLD='\e[1m'
# Resets
NORMAL='\e[0m'
#github-action genshdoc
#
# @file Post-Setup
# @brief Finalizing installation configurations and cleaning up after script.
echo -ne "$cyan
--------------------------------------------------------------------------
 █████╗ ██████╗  ██████╗██╗  ██╗███████╗███████╗████████╗██╗   ██╗██████╗ 
██╔══██╗██╔══██╗██╔════╝██║  ██║██╔════╝██╔════╝╚══██╔══╝██║   ██║██╔══██╗
███████║██████╔╝██║     ███████║███████╗█████╗     ██║   ██║   ██║██████╔╝
██╔══██║██╔══██╗██║     ██╔══██║╚════██║██╔══╝     ██║   ██║   ██║██╔═══╝ 
██║  ██║██║  ██║╚██████╗██║  ██║███████║███████╗   ██║   ╚██████╔╝██║     
╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚══════╝╚══════╝   ╚═╝    ╚═════╝ ╚═╝     

--------------------------------------------------------------------------
                      Automated Arch Linux Installer
                           SCRIPTHOME: ${lightyellow}ArchSetup
$cyan--------------------------------------------------------------------------
                      ${lightmagenta}Final Setup and Configurations
                   GRUB EFI Bootloader Install & Check
$cyan--------------------------------------------------------------------------
$NORMAL
"


source ${HOME}/ArchSetup/configs/setup.conf

if [[ -d "/sys/firmware/efi" ]]; then
    grub-install --efi-directory=/boot ${DISK}
fi

echo -ne "$cyan
--------------------------------------------------------------------------
                   ${lightmagenta}Creating and Theming Grub Boot Menu
$cyan--------------------------------------------------------------------------
$NORMAL
"
# set kernel parameter for decrypting the drive
if [[ "${FS}" == "luks" ]]; then
sed -i "s%GRUB_CMDLINE_LINUX_DEFAULT=\"%GRUB_CMDLINE_LINUX_DEFAULT=\"cryptdevice=UUID=${ENCRYPTED_PARTITION_UUID}:ROOT root=/dev/mapper/ROOT %g" /etc/default/grub
fi
# set kernel parameter for adding splash screen
sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT="[^"]*/& splash /' /etc/default/grub

echo -e "${lightyellow}Installing CyberRe Grub theme...$NORMAL"
THEME_DIR="/boot/grub/themes"
THEME_NAME=CyberRe
echo -e "${lightyellow}Creating the theme directory...$NORMAL"
mkdir -p "${THEME_DIR}/${THEME_NAME}"
echo -e "${lightyellow}Copying the theme...$NORMAL"
cd ${HOME}/ArchSetup
cp -a configs${THEME_DIR}/${THEME_NAME}/* ${THEME_DIR}/${THEME_NAME}
echo -e "${lightyellow}Backing up Grub config...$NORMAL"
cp -an /etc/default/grub /etc/default/grub.bak
echo -e "${lightyellow}Setting the theme as the default...$NORMAL"
grep "GRUB_THEME=" /etc/default/grub 2>&1 >/dev/null && sed -i '/GRUB_THEME=/d' /etc/default/grub
echo "GRUB_THEME=\"${THEME_DIR}/${THEME_NAME}/theme.txt\"" >> /etc/default/grub
echo -e "${lightyellow}Updating grub...$NORMAL"
grub-mkconfig -o /boot/grub/grub.cfg
echo -e "${lightgreen}All set!$NORMAL"

echo -ne "$cyan
--------------------------------------------------------------------------
                ${lightmagenta}Enabling and Theming Login Display Manager
$cyan--------------------------------------------------------------------------
$NORMAL
"
if [[ ${DESKTOP_ENV} == "kde" ]]; then
  systemctl enable sddm.service
  if [[ ${INSTALL_TYPE} == "FULL" ]]; then
    echo [Theme] >>  /etc/sddm.conf
    echo Current=Nordic >> /etc/sddm.conf
  fi

elif [[ "${DESKTOP_ENV}" == "gnome" ]]; then
  systemctl enable gdm.service

elif [[ "${DESKTOP_ENV}" == "lxde" ]]; then
  systemctl enable lxdm.service

elif [[ "${DESKTOP_ENV}" == "openbox" ]]; then
  systemctl enable lightdm.service
  if [[ "${INSTALL_TYPE}" == "FULL" ]]; then
    # Set default lightdm-webkit2-greeter theme to Litarvan
    sed -i 's/^webkit_theme\s*=\s*\(.*\)/webkit_theme = litarvan #\1/g' /etc/lightdm/lightdm-webkit2-greeter.conf
    # Set default lightdm greeter to lightdm-webkit2-greeter
    sed -i 's/#greeter-session=example.*/greeter-session=lightdm-webkit2-greeter/g' /etc/lightdm/lightdm.conf
  fi

else
  if [[ ! "${DESKTOP_ENV}" == "server"  ]]; then
  sudo pacman -S --noconfirm --needed lightdm lightdm-gtk-greeter
  systemctl enable lightdm.service
  fi
fi

echo -ne "$cyan
--------------------------------------------------------------------------
                       ${lightmagenta}Enabling Essential Services
$cyan--------------------------------------------------------------------------
$NORMAL
"
systemctl enable cups.service
echo -e "${lightyellow}  Cups enabled$NORMAL"
ntpd -qg
systemctl enable ntpd.service
echo -e "${lightyellow}  NTP enabled$NORMAL"
systemctl disable dhcpcd.service
echo -e "${lightyellow}  DHCP disabled$NORMAL"
systemctl stop dhcpcd.service
echo -e "${lightyellow}  DHCP stopped$NORMAL"
systemctl enable NetworkManager.service
echo -e "${lightyellow}  NetworkManager enabled$NORMAL"
systemctl enable bluetooth
echo -e "${lightyellow}  Bluetooth enabled$NORMAL"
systemctl enable avahi-daemon.service
echo -e "${lightyellow}  Avahi enabled$NORMAL"

if [[ "${FS}" == "luks" || "${FS}" == "btrfs" ]]; then

echo -ne "$cyan
--------------------------------------------------------------------------
                         ${lightmagenta}Creating Snapper Config
$cyan--------------------------------------------------------------------------
$NORMAL
"

SNAPPER_CONF="$HOME/ArchSetup/configs/etc/snapper/configs/root"
mkdir -p /etc/snapper/configs/
cp -rfv ${SNAPPER_CONF} /etc/snapper/configs/

SNAPPER_CONF_D="$HOME/ArchSetup/configs/etc/conf.d/snapper"
mkdir -p /etc/conf.d/
cp -rfv ${SNAPPER_CONF_D} /etc/conf.d/

fi

echo -ne "$cyan
--------------------------------------------------------------------------
                 ${lightmagenta}Enabling and Theming Plymouth Boot Splash
$cyan--------------------------------------------------------------------------
$NORMAL
"
PLYMOUTH_THEMES_DIR="$HOME/ArchSetup/configs/usr/share/plymouth/themes"
PLYMOUTH_THEME="arch-glow" # can grab from config later if we allow selection
mkdir -p /usr/share/plymouth/themes
echo -e "${lightyellow}Installing Plymouth theme..."
cp -rf ${PLYMOUTH_THEMES_DIR}/${PLYMOUTH_THEME} /usr/share/plymouth/themes
if  [[ $FS == "luks"]]; then
  sed -i 's/HOOKS=(base udev*/& plymouth/' /etc/mkinitcpio.conf # add plymouth after base udev
  sed -i 's/HOOKS=(base udev \(.*block\) /&plymouth-/' /etc/mkinitcpio.conf # create plymouth-encrypt after block hook
else
  sed -i 's/HOOKS=(base udev*/& plymouth/' /etc/mkinitcpio.conf # add plymouth after base udev
fi
plymouth-set-default-theme -R arch-glow # sets the theme and runs mkinitcpio
echo -e "${lightgreen}Plymouth theme installed"

echo -ne "$cyan
--------------------------------------------------------------------------
                               ${lightmagenta}Cleaning Up
$cyan--------------------------------------------------------------------------
$NORMAL
"
# Remove no password sudo rights
sed -i 's/^%wheel ALL=(ALL) NOPASSWD: ALL/# %wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers
sed -i 's/^%wheel ALL=(ALL:ALL) NOPASSWD: ALL/# %wheel ALL=(ALL:ALL) NOPASSWD: ALL/' /etc/sudoers
# Add sudo rights
sed -i 's/^# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers
sed -i 's/^# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/' /etc/sudoers

rm -r $HOME/ArchSetup
rm -r /home/$USERNAME/ArchSetup

# Replace in the same state
cd $pwd
